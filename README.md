### mactrack phone/computer WiFi client mac address -> SSID -> Street address PoC

[under construction but fully functional/ready to use- Don't mind the comments inside, updates coming]

Also identifies client brand name to help differentiate devices:
A reason to use WiPri to spoof mac/metadata - https://gitlab.com/Aresesi/wipri 

### FEATURES:
* Latest update now shows RSSI/signal strength on each packet 
- shows client mac address/brand name
- captures/listens/prints/isolates all saved network requests 
- OSINT database location lookup prompt to find SSID GPS/street address + logging to file w/date

Contact: righttoprivacy@tutanota.com

### QUICKSTART:
<<<<<<< HEAD
- check install script (for Phosh) variables
- check mact, mactrack variables/change wlan0 if that is not your card name
- to enable location search prompt add API code from wigle acct (see comments in mactrack)
- run install.sh
That's it. *** This will get updated, wanted to upload basic functionality first. ***

You can call mactrack as a command: sudo mactrack.
Or tap the "radar" icon I illustrated for it (if on Phosh).

### TO RUN ON STANDARD LINUX DESKTOP:
You may still use the install.sh to add mactrack as command
To use: mactrack 
(substitute wlan0 inside mactrack for your device if different)
=======
- check install.sh (for Phosh) variable location for Desktop files:
	(Tested on Mobian/Arch (DanctNIX) - let me know if you test it, and will add your distro)
- check mactrack variables/change wlan0 if that is not your card name
- to enable location search prompt add API code from wigle acct (see comments in mactrack)
- run install.sh

Now just tap the icon when you are ready to watch disconnected clients search for their saved WiFi Networks. This can be used to identify owners of devices, create redteam fake hotspots for autoconnect, check if you have devices with disconnect problems (can leak handshakes) and other uses (see blog for future ideas/updates) 

That's it.
>>>>>>> 8cd9d8c4b96bb3938e48b1e3722b22a418f474cb

### MACtrack: Identify visitors/ppl (including masked burglars) using WiFi phone/pc metadata

### Links:
<<<<<<< HEAD
Blog/Details:
https://politictech.wordpress.com
https://www.buymeacoffee.com/politictech/posts
=======
Blog/Details/Update:
https://www.buymeacoffee.com/politictech/posts
>>>>>>> 8cd9d8c4b96bb3938e48b1e3722b22a418f474cb

### My Video Channel Tutorials/Demos (free help in comments ;):
ODYSEE: https://odysee.com/$/invite/@RTP:9 (invite link helps my channel, Odysee really nice place to share files + videos including mactrack)
PEERTUBE: https://tube.tchncs.de/video-channels/privacy__tech_tips (Peertube decentralized platform)
BITCHUTE: https://www.bitchute.com/channel/IHbpKZeUrxnI/ (Alternative mirror)
YOUTUBE: https://www.youtube.com/channel/UChVCEXzi39_YEpUQhqmEFrQ

***There may come a time Youtube may no longer appreciates me (if they do now :-P ) please subscribe to my Odysee (some videos not elsewhere)

### Basic Use:  (setup wigle account if you want to use SSID lookup prompt (for identifying client mac/brand/ssid/rssi wigle not needed)

***See: https://www.buymeacoffee.com/politictech/mactrack-howto-use-it-identify-masked-burglars-strangers-outside-your-home for more details, cut/paste 
    directions and solutions to preven you being tracked by mactrack.
# Run the install.sh script to install: be sure to set variables to your OS if needed

1.) Make sure you have all modules called for/important at top of script: OuiLookup, yawigle, scapy
    to be sure simply run: pip3 install OuiLookup && pip3 install yawigle && pip3 install scapy
    Now you should be good to go.

2.) Run mactrack.

3.) Hit ctrl+c to drop an SSID gps/street address lookup prompt

### What does it do?

It prints out all WiFi clients in the area looking for previously saved wifi networks (think about how 
many places you ever saved a WiFi network- all those SSID's will be printed on screen with your wifi mac 
address pointed at it + RSSI; Then the SSID prompt lets you find all records of GPS/street address locations for 
that SSID;

This can be used for various purposes- ie: identify a masked burglar.
